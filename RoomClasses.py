#Room classes
import random
from monsterbuilder import Monster

class Room():
    
    def __init__(self):
        styles = ["TreasureRoom", "Boss", "MonsterRoom", "RespawnRoom"]
        items = ["BigStick", "WoodenSword", "StoneAxe", "StoneMace", "IronSword", "IronAxe", "IronMace", "SpikedClub", "Armor Upgrade"]
        self.style = styles[random.randrange(len(styles)-1)]
        self.gold = random.randrange(21)
        self.monsters = []
        monsternum = random.randrange(4)
        for i in range(monsternum):
            self.monsters.append(Monster())
        
        self.totalEXP = 0
        for monster in self.monsters:
            self.totalEXP += monster.EXP
        
        if(self.style == "TreasureRoom"):
            
            randgold = random.randrange(2)
            if(randgold == 1):
                self.gold = random.randrange(1000)
            else:
                self.gold = self.gold + 10
                
        self.items = []        
        self.itemnum = random.randrange(4)
        for i in range(self.itemnum):
            self.items.append(items[random.randrange(len(items))])
            
        '''print("style: {0}".format(self.style))
        print("gold: {0}".format(self.gold))
        print("itemnum: {0}".format(self.itemnum))
        print("items: {0}".format(self.items))'''
        
        self.buildVisual()
        
    def buildVisual(self):
        self.xdim = 10
        self.ydim = 5
        self.grid = []
        topAndBottomGrid = []
        for i in range(self.xdim + 1):
            topAndBottomGrid.append("-")
        self.grid.append(topAndBottomGrid)
        for i in range(self.ydim):
            newInteriorList = ["|"]
            for x in range(self.xdim):
                newInteriorList.append(" ")
            newInteriorList.append("|")
            self.grid.append(newInteriorList)
        self.grid.append(topAndBottomGrid)
        del(newInteriorList)
        
        # Now add the items and gold to the room
        for i in self.items:
            if ("Mace" in i):
                theSymbol = "*"
            elif ("Stick" in i):
                theSymbol = "\\"
            elif ("Sword" in i):
                theSymbol = "S"
            elif ("Axe" in i):
                theSymbol = "A"
            elif ("Club" in i):
                theSymbol = "P"
            else:
                theSymbol = "  "
                
            done = False
            while (done == False):
                tempx = random.randrange(self.xdim)
                tempy = random.randrange(self.ydim)
                
                if (self.grid[tempy][tempx] == " "):
                    self.grid[tempy][tempx] = theSymbol
                    done = True
                    
        done = False
        while (done == False):
            tempx = random.randrange(self.xdim)
            tempy = random.randrange(self.ydim)
            if (self.grid[tempy][tempx] == " "):
                self.grid[tempy][tempx] = "G"
                done = True
            
    def render(self):
        tempPrint = ""
        for i in self.grid:
            for x in i:
                tempPrint += x
            tempPrint += "\n"
        print(tempPrint)
        
r = Room()