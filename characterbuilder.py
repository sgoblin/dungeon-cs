# Character Builder by Ryan

import random
from faker import Faker

fake = Faker()

abilities = ["sneaking", "stealing", "cooking", "alchemy", "woodcarving", "alchemy", "singing", "fire_magic", "healing_magic", "medicine", "necromancy_magic", "charm_magic", "tailoring", "blacksmithing", "competitive_eating", "arm_wrestling", "spitting", "bar_games", "brewing", "swordfighting", "spellcasting", "archery", "shuriken", "throwing_knives", "fencing", "shieldbearing", "bluffing", "poetry", "riddles"]

fears = ["spiders", "snakes", "women", "children", "darkness", "heights", "low_ceilings", "wet_things", "weird_noises"]

genders = ["male", "female"]

male_names = ["Prince Garnotin", "Knight Walkelin", "Emperor Fulko", "Serf Yvet", "Baron Ernaut", "Serf Ferri", "Reeve Remont", "Margrave Geronim", "Viscount Jewell", "Grand Duke Gilebert", "Alderman Batty", "Monk Bertylmew", "Margrave Gervesot", "Baronet Raimond", "Margrave Wymarc", "Squire Cristoffle", "Count Jeremias", "Emperor Bernier", "Prince Herveus", "Cardinal Higg"]

female_names = ["Countess Genevieve", "Lady Moolde", "Princess Thiphania", "Maid Jennet", "Peasant Aveline", "Squire Rosomon", "Countess Anchoret", "Maid Joane", "Dame Elizibeth", "Peasant Motte", "Dame Evota", "Maiden Galienne", "Viscountess Esobel", "Lady in Waiting Isard", "Viscountess Hele", "Peasant Avice", "Squire Jeane", "Grand Duchess Izett", "Maiden Ismenia", "Maid Christiania"]

def intInput(message):
    done = False
    while(done == False):
        try:
            response = int(input(message))
            done = True
            return response
            break
        except ValueError:
            print("That was not a number. Try again.")

class Character():
    def __init__(self):
        # Define the gender
        temp = random.randrange(2)
        if (temp == 1):
            self.gender = "male"
        else:
            self.gender = "female"

        # Define the name
        if (self.gender == "male"):
            '''temp = random.randrange(len(male_names))
            self.name = male_names[temp]'''
            self.name = fake.name_male()
        else:
            '''temp = random.randrange(len(female_names))
            self.name = female_names[temp]'''
            self.name = fake.name_female()

        # Define the intelligence, damage and nerve
        self.intelligence = random.randrange(11)
        self.nerve = random.randrange(11)
        self.damage = random.randrange(11)
        self.armor = random.randrange(11)
        self.health = random.randrange(5, 26)
        self.explevel = random.randrange(2)

        # Fears
        numFear = random.randrange(len(fears))
        self.fears = []
        for i in range(numFear):
            temp = fears[random.randrange(len(fears))]
            if not temp in self.fears:
                self.fears.append(temp)

        # Skills
        numSkill = random.randrange(len(abilities))
        self.skills = []
        for i in range(numFear):
            temp = abilities[random.randrange(len(abilities))]
            if not temp in self.skills:
                self.skills.append(temp)

        # Misc data
        self.job = fake.job()
        self.city = fake.city()

    def render(self):
        possibleTitles = ["The Majestic, Glorious, and Fantastic"]
        print("The Majestic, Glorious, and Fantastic " + self.job + "...")
        print(self.name + " of " + self.city + "!")
        print("Intelligence: " + str(self.intelligence) + " out of 10!")
        print("Damage: " + str(self.damage) + " out of 10!")
        print("Nerve: " + str(self.nerve) + " out of 10!")
        print("Armor: " + str(self.armor) + " out of 10!")
        print("Health: " + str(self.health) + " out of 25!")
        print("Skills: ", end="")
        for i in self.skills:
            print(i, end=", ")
        print(" And that's it folks!\nFears: ", end="")
        for i in self.fears:
            print(i, end=", ")
        print(" And that's it, folks!\n")

def main():
    howMany = intInput("How many adventurers would you like to generate? ")
    theCharacters = []
    for i in range(howMany):
        theCharacters.append(Character())
    print("Your characters, sir! \n")
    for i in theCharacters:
        i.render()

if (__name__ == "__main__"):
    main()
