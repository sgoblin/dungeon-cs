# The file containing the Party class

import characterbuilder
from monsterbuilder import Monster
from time import sleep

numberOfAdventurers = 3

class Party():
    def __init__(self):
        self.adventurers = []
        for i in range(numberOfAdventurers):
            self.adventurers.append(characterbuilder.Character())
        #for i in self.adventurers:
        #    i.render()
        self.gold = 0
        self.items = []
        self.calculateTotals()
        
    def calculateTotals(self):
        self.totalIntelligence = 0
        self.totalDamage = 0
        self.totalNerve = 0
        self.totalEXP = 0
        self.totalHealth = 0
        self.totalArmor = 0
        for i in self.adventurers:
            self.totalIntelligence += i.intelligence
            self.totalDamage += i.damage
            self.totalNerve += i.nerve
            self.totalArmor += i.armor
            self.totalHealth += i.health
            self.totalEXP += i.explevel
    
    def render(self):
        print("Your massively awesome party of adventurers doing stuff consists of: \n")
        for i in self.adventurers:
            i.render()
            
    def enterRoom(self, room):
        if (len(self.adventurers) == 0):
            print("Sorry, all of your adventurers have died. Please return again so that we can strip the flesh off of your bones!")
            exit()
        print("You have entered a room with: " + str(room.gold) + " gold.")
        room.render()
        self.gold += room.gold
        room.gold = 0
        
        # Do monster combat now
        for monster in room.monsters:
            self.fightMonster(monster)
        room.monsters = []
        if (len(self.adventurers) == 0):
            print("Sorry, all of your adventurers have died. Please return again so that we can strip the flesh off of your bones!")
            exit()
            
        # Gain stuff
        for i in room.items:
            self.items.append(i)
            print("You have gained a(n): " + i)
        for i in range(len(self.items)):
            if(self.items[i] == "Armor Upgrade"):
                self.totalArmor += self.totalArmor*.2
                self.items[i] = "Used Armor Upgrade"
                print("You got an Armor Upgrade!")
                
        if (room.totalEXP > 0):
            self.totalEXP += room.totalEXP
            print("You have gained " + str(room.totalEXP) + " EXP.")
        
        room.items = []
        print("Congratulations, you now have: " + str(self.gold) + " gold!")
        room.buildVisual()
        return room
    
    def level(self):
        if(self.totalEXP >= 10):
          self.totalIntelligence += self.totalIntelligence*.1
          self.totalDamage += self.totalDamage*.1
          self.totalNerve += self.totalNerve*.1
          self.totalHealth += self.totalHealth*.1
          print("Your party has leveled up to level 1!")
          print("Your Party's atributes have gone up by 10%")
        if(self.totalEXP >= 15):
          self.totalIntelligence += self.totalIntelligence*.1
          self.totalDamage += self.totalDamage*.1
          self.totalNerve += self.totalNerve*.1
          self.totalHealth += self.totalHealth*.1
          print("Your party has leveled up to level 2!")
          print("Your Party's atributes have gone up by 10%")
        if(self.totalEXP >= 22):
          self.totalIntelligence += self.totalIntelligence*.1
          self.totalDamage += self.totalDamage*.1
          self.totalNerve += self.totalNerve*.1
          self.totalHealth += self.totalHealth*.1
          print("Your party has leveled up to level 3!")
          print("Your Party's atributes have gone up by 10%")
        if(self.totalEXP >= 33):
          self.totalIntelligence += self.totalIntelligence*.1
          self.totalDamage += self.totalDamage*.1
          self.totalNerve += self.totalNerve*.1
          self.totalHealth += self.totalHealth*.1
          print("Your party has leveled up to level 4!")
          print("Your Party's atributes have gone up by 10%")
        if(self.totalEXP >= 49):
          self.totalIntelligence += self.totalIntelligence*.1
          self.totalDamage += self.totalDamage*.1
          self.totalNerve += self.totalNerve*.1
          self.totalHealth += self.totalHealth*.1
          print("Your party has leveled up to level 5!")
          print("Your Party's atributes have gone up by 10%")
        if(self.totalEXP >= 73):
          self.totalIntelligence += self.totalIntelligence*.1
          self.totalDamage += self.totalDamage*.1
          self.totalNerve += self.totalNerve*.1
          self.totalHealth += self.totalHealth*.1
          print("Your party has leveled up to level 6!")
          print("Your Party's atributes have gone up by 10%")
        if(self.totalEXP >= 109):
          self.totalIntelligence += self.totalIntelligence*.1
          self.totalDamage += self.totalDamage*.1
          self.totalNerve += self.totalNerve*.1
          self.totalHealth += self.totalHealth*.1
          print("Your party has leveled up to level 7!")
          print("Your Party's atributes have gone up by 10%")
        if(self.totalEXP >= 163):
          self.totalIntelligence += self.totalIntelligence*.1
          self.totalDamage += self.totalDamage*.1
          self.totalNerve += self.totalNerve*.1
          self.totalHealth += self.totalHealth*.1
          print("Your party has leveled up to level 8!")
          print("Your Party's atributes have gone up by 10%")
        if(self.totalEXP >= 244):
          self.totalIntelligence += self.totalIntelligence*.1
          self.totalDamage += self.totalDamage*.1
          self.totalNerve += self.totalNerve*.1
          self.totalHealth += self.totalHealth*.1
          print("Your party has leveled up to level 9!")
          print("Your Party's atributes have gone up by 10%")
        if(self.totalEXP >= 366):
          self.totalIntelligence += self.totalIntelligence*.1
          self.totalDamage += self.totalDamage*.1
          self.totalNerve += self.totalNerve*.1
          self.totalHealth += self.totalHealth*.1
          print("Your party has leveled up to level 10!")
          print("Your Party's atributes have gone up by 10%")
            
    def fightMonster(self, monster):
        for character in self.adventurers:
            # Combat mechanics
            print("My name is " + character.name + " and I shall triumph over the " + monster.name + "!")
        
        print("RAWR!\n")
        monster.render()
        
        print("\nFOR THE GLORY OF THE SONTARAN EMPIRE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
        partyDamageTaken = 0
        monsterDamageTaken = 0
        
        while (partyDamageTaken <= self.totalHealth and monsterDamageTaken < monster.health):
            print("Party attacking!")
            sleep(0.2)
            tempMonsterDamage = (self.totalDamage * (self.totalNerve/5))-(monster.armor/5)
            monsterDamageTaken += tempMonsterDamage
            print("Monster has taken " + str(tempMonsterDamage) + " damage and currently has " + str(monster.health - monsterDamageTaken) + " health.\nMonster attacking!")
            sleep(0.2)
            tempPartyDamage = (monster.damage * (self.totalNerve/5))-(self.totalArmor/5)
            partyDamageTaken += tempPartyDamage
            print("Party has taken " + str(tempPartyDamage) + " damage. Party health is currently " + str(self.totalHealth - partyDamageTaken) + " health.")
        
        print("\nLet us count the dead.")
        for adventurer in self.adventurers:
            if (adventurer.health < partyDamageTaken):
                print("Oh no! " + adventurer.name + " is dead! Please pause for a moment of silence and let us remember: \n")
                adventurer.render()
                sleep(5)
                self.adventurers.remove(adventurer)
                partyDamageTaken -= adventurer.health
            else:
                print("Fantastic! " + adventurer.name + " has not died!\n")
                
        self.calculateTotals()
               