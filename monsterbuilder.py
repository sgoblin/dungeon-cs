import random
import pickle

class Monster:
    health = 0
    name = ""
    armor = 0
    damage = 0
    poison = 0
    EXP = 0
    
    with open("visuals.pickle", "rb") as picklefile:
        visuals = pickle.load(picklefile)

    def __init__(self):
        MonsterName = ["RabOrc", "Goblin", "Dragon","Smith","Sergio","Slime","Spider","Zombie","Gryphon","Skeleton","LandKraken","LandWhale"]
        
        self.type = MonsterName[random.randrange(len(MonsterName))]
        self.name = self.type
        self.health = 15 + random.randrange(16)
        self.armor = 8 + random.randrange(10)
        self.damage = 1 + random.randrange(8)
        self.poison = 0 + random.randrange(11)
        self.EXP = 1 + random.randrange(30)
        self.boss = False
        
        if(self.health>=25):
            self.name  = "Giant {0}".format(self.name)
        
        if(self.armor>=14):
            self.name = "Tanky {0}".format(self.name)
            
        if(self.damage>=6):
            self.name = "Super {0}".format(self.name)
            
        if(self.poison>=9):
            self.name = "Poisonous {0}".format(self.name)
            
        if(self.armor == 30):
            self.name = "Mecha {0}".format(self.name)
            
        if(self.health>=30 and self.armor>=17 and self.damage>=8 and self.poison>=10):
            self.name = "Boss {0}".format(self.name)
            self.boss = True
            
        if (self.type in self.visuals.keys()):
            self.visual = self.visuals[self.type]
        else:
            self.visual = ""
            
    def render(self):
        print("Name: ",self.name)
        print("Health: ",self.health)
        print("Armor: ",self.armor)
        print("Damage: ",self.damage)
        print("This monster is worth",self.EXP,"EXP")
        print(self.visual)

#s = Monster()
#s.render()