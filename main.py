# Dungeon Crawler by Isaiah and Ryan

import random
import monsterbuilder
from RoomClasses import Room
from Party import Party

def intInput(message):
    done = False
    while(done == False):
        try:
            response = int(input(message))
            done = True
            return response
            break
        except ValueError:
            print("That was not a number. Try again.")
            
def genDungeon(levels, rooms):
    dungeon = []
    for i in range(levels):
        dungeon.append([])
    for i in dungeon:
        for axe in range(rooms):
            i.append(Room())
    return dungeon
    
def main():
    print("Welcome to THE DUNGEON! Let us venture into darkness...")
    theParty = Party()
    theParty.render()
    
    longOrShort = intInput("Do you want to go on a nice short dungeon trip (0) or a long and dangerous one (1)? ")
    # The short and fun dungeon
    if (longOrShort == 0):
        dungeon = genDungeon(1, 4)
        dungeonLevel = 0
        dungeonRoom = -1
        while(len(dungeon) > dungeonLevel):
            while (dungeonRoom < len(dungeon[dungeonLevel])):
                forwardOrBack = intInput("Would you like to enter into a new room (0) or turn back (1)? ")
                if (forwardOrBack == 0):
                    if (dungeonRoom+1 < len(dungeon[dungeonLevel])):
                        dungeonRoom += 1
                        print("Welcome to room #" + str(dungeonRoom))
                        dungeon[dungeonLevel][dungeonRoom] = theParty.enterRoom(dungeon[dungeonLevel][dungeonRoom])
                    else:
                        dungeonRoom += 1
                elif (forwardOrBack == 1 and dungeonRoom > 0):
                    dungeonRoom -= 1
                    print("Welcome to room #" + str(dungeonRoom))
                    dungeon[dungeonLevel][dungeonRoom] = theParty.enterRoom(dungeon[dungeonLevel][dungeonRoom])
                else:
                    print("Try going forward, as you are now back at the beginning of this level.")
            dungeonLevel += 1
        print("You exit the dungeon into the cool air outside, reveling in your newfound freedom. You shall live as a king with the " + str(theParty.gold) + " gold that you found!")
        
    # The long and dangerous dungeon!
    elif (longOrShort == 1):
        rooms = []
        dangerValue = 0
        
        done = False
        while(done == False):
            rooms.append(Room())
            print("You see a new room.")
            nextChoice = intInput("Do you wish to continue into room #" + str(dangerValue) + str(len(rooms)) + " (0), continue along the corridor (1), or run away (2)? \n>>> ")
            if (nextChoice == 2):
                print("We're sorry to see you go! Please come again so that we may eat the flesh off of your bones!")
                break
            elif (nextChoice == 1):
                pass
            else:
                theParty.enterRoom(rooms[-1])
                nextChoice = intInput("Do you wish to return to the previous corridor (0) or go to the new one (1)? \n>>> ")
                if (nextChoice != 0):
                    dangerValue += 1
                    print("You have gone to the next corridor! More risk, more reward.")
                else:
                    print("You have returned to the previous corridor.")
            
if __name__ == "__main__":
    main()