# Adds a visual file to the pickle

import pickle

def main():
    theFileName = "visuals/" + input("Please input the file name that you want to add to the pickle: ")
    theMonster = input("Please input the name of the monster that this represents: ")
    with open(theFileName, "r") as theFile:
        theData = theFile.read()
    
    with open("visuals.pickle", "rb") as pickleFile:
        theList = pickle.load(pickleFile)
    print(theList)
    with open("visuals.pickle", "wb") as pickleFile:
        theList[theMonster] = theData
        pickle.dump(theList, pickleFile)
        
if (__name__ == "__main__"):
    main()